import {getAllSuperheroes} from '../services/gameService.js'


const fetchAllSuperheroes = async () => {
    try {
        const results = await getAllSuperheroes()
        return results
    } catch (error) {

    }
}

export {fetchAllSuperheroes}
