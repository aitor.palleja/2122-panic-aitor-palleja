import {fetchAllSuperheroes} from "./controller/gameController.js";

let allSuperHeroArray = [];
let villainZarate = [];
let superHero = [];

fetchAllSuperheroes()
.then(res => {
    allSuperHeroArray = JSON.parse(JSON.stringify(res));
    //console.log(allSuperHeroArray)

// Añadir atributo hitpoints a los heroes. hitPoints = hitpoints = strength * 5. Max 300.
allSuperHeroArray.forEach(element => {
    let hitPoints = element.powerstats.strength * 5;
        if(hitPoints > 300)
        {
           hitPoints = 300;
        }
        element.powerstats["hitPoints"] = hitPoints;     
    })

//Crear número random
const randomNumber = Math.floor(Math.random() * allSuperHeroArray.length) +1
    if(randomNumber === 4)
    {
        rand = Math.floor(Math.random() * allSuperHeroArray.length) +1
    }

//Buscar villano con ID 4 y guardarlo en objeto villainZarate
//Buscar heroe random y guardarlo en el objeto superHeroe

allSuperHeroArray.forEach(element => {
    if(element.id === 4)
    {
        villainZarate = element;
    }

    if(element.id === randomNumber)
    {
        superHero = element;
    }
})

console.log("El villano es " + villainZarate.name)
console.log("Estadisticas")
console.log(villainZarate.powerstats)
console.log("El superheroe es " + superHero.name)
console.log("Estadisticas")
console.log(superHero.powerstats)

//Calcular velocidad de los jugadores para ver quien empieza el combate 

let sumVillainIntSpeed = villainZarate.powerstats.intelligence + villainZarate.powerstats.speed;
let sumHeroIntSpeed = superHero.powerstats.intelligence + superHero.powerstats.speed;
let fastestPlayer = [];
let slowestPlayer = [];

if(sumHeroIntSpeed > sumVillainIntSpeed)
{
    fastestPlayer = superHero;
    slowestPlayer = villainZarate;
}
    else
    {
        fastestPlayer = villainZarate;
        slowestPlayer = superHero;
    }

//console.log(dado)

/*Lanzamos 1D100 y si el resultado es igual o menor que combat el
ataque tiene ÉXITO. Si el ataque es exitoso se puede bloquear,  si el poder 
de bloqueo es mayor que el dado de bloqueo se defiendo y -3 durabilidad.
block = (speed + combat) / 2 */

// Creamos los dados

const attackD100 = Math.floor(Math.random() * (100-1 +1));
const blockD100 = Math.floor(Math.random() * (100-1 +1));
const D20 = Math.floor(Math.random() * (20-1 +1));

//Sacamos datos de los atributos de los objetos a variables para manejarlos de manera más sencilla

let fastestPlayerBlockPower = Math.ceil((fastestPlayer.powerstats.speed + fastestPlayer.powerstats.combat) / 2);
let slowestPlayerBlockPower = Math.ceil((slowestPlayer.powerstats.speed + slowestPlayer.powerstats.combat) / 2);
let fastestPlayerHitPoints = fastestPlayer.powerstats.hitPoints;
let slowestPlayerHitPoints = slowestPlayer.powerstats.hitPoints

console.log(fastestPlayer.name + " es el más rápido, empieza turno")

let fastestPlayerWeaponDurability = fastestPlayer.powerstats.durability
if (attackD100 <= fastestPlayer.powerstats.combat)
{
    console.log("El ataque de " + fastestPlayer.name + " ha sido exitoso")

    if(blockD100 <= slowestPlayerBlockPower)
    {
        console.log(slowestPlayer.name + " ha conseguido bloquear el ataque")
        fastestPlayerWeaponDurability = fastestPlayerWeaponDurability - 3;
        console.log("El arma de " + fastestPlayer.name + " pierde 3 de durabilidad, le quedan " + fastestPlayerWeaponDurability)
    } 
    
        else 
        {
            console.log(slowestPlayer.name + " no ha podido bloquear el ataque")
            let fastestPlayerWeaponAttack = Math.ceil((fastestPlayer.powerstats.power * fastestPlayer.powerstats.durability) / 100);
            let fastestPlayerRealWeaponAttack = Math.ceil((fastestPlayerWeaponAttack + fastestPlayer.powerstats.strength)* D20 / 100);
            slowestPlayerHitPoints = slowestPlayerHitPoints - fastestPlayerRealWeaponAttack;
            console.log(slowestPlayer.name + " recibe " + fastestPlayerRealWeaponAttack + " de daño y le quedan " + slowestPlayerHitPoints + " puntos de vida" )
            fastestPlayerWeaponDurability = fastestPlayerWeaponDurability - 1
            console.log(fastestPlayer.name + " pierde 1 punto de durabilidad en su arma, le quedan " + fastestPlayerWeaponDurability)
        }
} 
    else
    {
        console.log("ataque fallido")
    }
})

