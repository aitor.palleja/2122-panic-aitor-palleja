import axios from 'axios';

const getAllSuperheroes = async () => {
    //Request API
    return axios.get('https://cdn.jsdelivr.net/gh/akabab/superhero-api@0.3.0/api/all.json')
    .then(function (response) {
        //handle success
        return response.data;
    })
    .catch(function (error) {
        //handle error
        console.log(error);
}) 
}

export {getAllSuperheroes}
